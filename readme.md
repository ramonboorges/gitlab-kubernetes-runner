# Installation steps

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade -i -n gitlab-runner gitlab/gitlab-runner --version 0.43.1 --values custom-values.yaml
```
